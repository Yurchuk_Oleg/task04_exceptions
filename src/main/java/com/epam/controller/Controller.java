package com.epam.controller;

import com.epam.exception.WrongValueForPriceException;
import com.epam.model.Computer;

public class Controller {
    public void isPriceValid(Computer computer) throws WrongValueForPriceException {
        if (computer.getPrice() < 0) {
            throw new WrongValueForPriceException();
        }
    }
}
