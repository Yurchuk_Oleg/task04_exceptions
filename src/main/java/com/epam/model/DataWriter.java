package com.epam.model;

import com.epam.exception.WrongDataWriteException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class DataWriter implements AutoCloseable {
    private BufferedWriter writer;
    private String strData;
    String fileName = "Data.txt";

    public DataWriter() throws IOException {
        writer = new BufferedWriter(new FileWriter(fileName));
    }

    public void writeDataIntoFile(Computer computer){
        strData += computer.toString();
    }

    public void close() throws Exception {
        try{
            writer.write(strData);
            writer.close();
        } catch (WrongDataWriteException e){
            throw new WrongDataWriteException();
        }
    }
}
