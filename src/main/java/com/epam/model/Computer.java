package com.epam.model;

public class Computer {
    private String name;
    private int price;
    private int processors;
    private int ram;

    public Computer(String name, int price, int processors, int ram) {
        this.name = name;
        this.price = price;
        this.processors = processors;
        this.ram = ram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProcessors() {
        return processors;
    }

    public void setProcessors(int processors) {
        this.processors = processors;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", processors=" + processors +
                ", ram=" + ram +
                '}';
    }
}
