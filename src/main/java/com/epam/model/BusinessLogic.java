package com.epam.model;

import com.epam.controller.Controller;
import com.epam.exception.WrongDataWriteException;
import com.epam.exception.WrongValueForPriceException;

import java.io.IOException;

public class BusinessLogic {
    private Controller controller = new Controller();
    private Computer comp1 = new Computer("Lenovo",13000,2,8);
    private Computer comp2 = new Computer("Acer",15000,4,8);

    private void isPriceValid(){
        try{
            controller.isPriceValid(comp1);
            controller.isPriceValid(comp2);
        } catch (WrongValueForPriceException e) {
            e.printStackTrace();
        }
    }

    private void writeDataIntoFile() throws Exception {
        try (DataWriter dataWriter = new DataWriter()) {
            dataWriter.writeDataIntoFile(comp1);
            dataWriter.writeDataIntoFile(comp2);
        } catch (WrongDataWriteException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        BusinessLogic businessLogic = new BusinessLogic();
        businessLogic.isPriceValid();
        businessLogic.writeDataIntoFile();
    }
}
